// threads2.rs
//
// Building on the last exercise, we want all of the threads to complete their
// work but this time the spawned threads need to be in charge of updating a
// shared value: JobStatus.jobs_completed
//
// Execute `rustlings hint threads2` or use the `hint` watch subcommand for a
// hint.

use std::sync::{Arc, Mutex};
use std::thread;
use std::time::Duration;

struct JobStatus {
    jobs_completed: u32,
}

fn main() {
    let initial_jobs = JobStatus { jobs_completed: 0 };
    let mutex_jobs = Mutex::new(initial_jobs);
    let main_status = Arc::new(mutex_jobs);
    let mut handles = vec![];
    for _ in 0..10 {
        let status_shared = Arc::clone(&main_status);
        let handle = thread::spawn(move || {
            thread::sleep(Duration::from_millis(250));
            let mut shared_jobs = status_shared.lock().unwrap();
            shared_jobs.jobs_completed += 1;
        });
        handles.push(handle);
    }
    for handle in handles {
        handle.join().unwrap();
        println!("jobs completed {:?}", main_status.lock().unwrap().jobs_completed);
    }
}
